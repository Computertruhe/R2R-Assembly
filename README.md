<img align="right" width="200" height="200" src="https://codeberg.org/repo-avatars/15745-9d1b283b33755c9a9cdf466590466c9d">

# DiVOC-Assembly

## Intro
Hier finden sich unsere *DiVOC*-Maps. Dies sind 2D-Karten, die als Basis für die virtuelle Welt der *DiVOC *-Veranstaltung dienen, in der man mit anderen Wesen erstmals während des *DiVOC* im Jahr 2021 interagieren konnte und in der wir die *Computertruhe*-Assembly repräsentieren.

Der *CCC* stellt dankenswerterweise weitere Informationen rund um die [Erstellung und Integration der Karten](https://di.c3voc.de/howto:world) zur Verfügung.

Die virtuelle *DiVOC*-Umgebung basiert auf *[Work Adventure](https://workadventu.re)*. Wie eine solche 2D-Welt aussehen kann, lässt sich [hier](https://play.staging.workadventu.re/) begutachten.

## Struktur des Repos
### Root
Auf der obersten Ebene liegen die finalen, direkt auslieferbaren Karten im JSON-Format inkl. der referenzierten Assets in den Unterordnern `sounds` und `tilesets`. Laut Konvention des *CCC* muss sich auf dieser Ebene die `main.json`-Einstiegskarte befinden.

Außerdem finden sich im Ordner `webpages` kleine, ansprechende Webseiten zum Öffnen in der 2D-Welt. Sie dienen u. a. zur Anzeige von Fotos und diversen Textinformationen.

### Assets
* Alle Sound-Dateien werden unter `/Assets/Sounds` abgelegt.
* Tilesets lagern in separaten Unterordnern im Ordner `/Assets/Tilesets`.