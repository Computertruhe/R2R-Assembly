<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.7.2" name="Dreikopfaffe" tilewidth="64" tileheight="64" tilecount="15" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="64" height="64" source="00.png"/>
  <animation>
   <frame tileid="0" duration="100"/>
   <frame tileid="1" duration="100"/>
   <frame tileid="2" duration="100"/>
   <frame tileid="3" duration="100"/>
   <frame tileid="4" duration="100"/>
   <frame tileid="5" duration="100"/>
   <frame tileid="6" duration="100"/>
   <frame tileid="7" duration="100"/>
   <frame tileid="8" duration="100"/>
   <frame tileid="9" duration="100"/>
   <frame tileid="10" duration="100"/>
   <frame tileid="11" duration="100"/>
   <frame tileid="12" duration="100"/>
   <frame tileid="13" duration="100"/>
   <frame tileid="14" duration="100"/>
  </animation>
 </tile>
 <tile id="1">
  <image width="64" height="64" source="01.png"/>
 </tile>
 <tile id="2">
  <image width="64" height="64" source="02.png"/>
 </tile>
 <tile id="3">
  <image width="64" height="64" source="03.png"/>
 </tile>
 <tile id="4">
  <image width="64" height="64" source="04.png"/>
 </tile>
 <tile id="5">
  <image width="64" height="64" source="05.png"/>
 </tile>
 <tile id="6">
  <image width="64" height="64" source="06.png"/>
 </tile>
 <tile id="7">
  <image width="64" height="64" source="07.png"/>
 </tile>
 <tile id="8">
  <image width="64" height="64" source="08.png"/>
 </tile>
 <tile id="9">
  <image width="64" height="64" source="09.png"/>
 </tile>
 <tile id="10">
  <image width="64" height="64" source="10.png"/>
 </tile>
 <tile id="11">
  <image width="64" height="64" source="11.png"/>
 </tile>
 <tile id="12">
  <image width="64" height="64" source="12.png"/>
 </tile>
 <tile id="13">
  <image width="64" height="64" source="13.png"/>
 </tile>
 <tile id="14">
  <image width="64" height="64" source="14.png"/>
 </tile>
</tileset>
