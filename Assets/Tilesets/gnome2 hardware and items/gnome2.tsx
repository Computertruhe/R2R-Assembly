<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.3" name="gnome2" tilewidth="32" tileheight="32" tilecount="127" columns="0">
 <grid orientation="orthogonal" width="10" height="10"/>
 <tile id="222">
  <image width="32" height="32" source="boot-switch-large.png"/>
 </tile>
 <tile id="223">
  <image width="32" height="32" source="boot-switch.png"/>
 </tile>
 <tile id="224">
  <image width="32" height="32" source="computer.png"/>
 </tile>
 <tile id="225">
  <image width="32" height="32" source="computer.small.png"/>
 </tile>
 <tile id="226">
  <image width="32" height="32" source="config-xfree.png"/>
 </tile>
 <tile id="227">
  <image width="32" height="32" source="emblem-art.png"/>
 </tile>
 <tile id="228">
  <image width="32" height="32" source="emblem-camera-small.png"/>
 </tile>
 <tile id="229">
  <image width="32" height="32" source="emblem-camera.png"/>
 </tile>
 <tile id="230">
  <image width="32" height="32" source="emblem-documents.png"/>
 </tile>
 <tile id="231">
  <image width="32" height="32" source="emblem-marketing.png"/>
 </tile>
 <tile id="232">
  <image width="32" height="32" source="emblem-money.png"/>
 </tile>
 <tile id="233">
  <image width="32" height="32" source="emblem-note.png"/>
 </tile>
 <tile id="234">
  <image width="32" height="32" source="emblem-package.png"/>
 </tile>
 <tile id="235">
  <image width="32" height="32" source="emblem-trash.png"/>
 </tile>
 <tile id="236">
  <image width="32" height="32" source="gdm.png"/>
 </tile>
 <tile id="237">
  <image width="32" height="32" source="gnome-applications.png"/>
 </tile>
 <tile id="238">
  <image width="32" height="32" source="gnome-blackjack.png"/>
 </tile>
 <tile id="239">
  <image width="32" height="32" source="gnome-calculator.png"/>
 </tile>
 <tile id="240">
  <image width="32" height="32" source="gnome-cardgame.png"/>
 </tile>
 <tile id="241">
  <image width="32" height="32" source="gnome-cd.png"/>
 </tile>
 <tile id="242">
  <image width="32" height="32" source="gnome-control-center.png"/>
 </tile>
 <tile id="243">
  <image width="32" height="32" source="gnome-cpu.png"/>
 </tile>
 <tile id="244">
  <image width="32" height="32" source="gnome-dev-cdrom.png"/>
 </tile>
 <tile id="245">
  <image width="32" height="32" source="gnome-dev-floppy.png"/>
 </tile>
 <tile id="246">
  <image width="32" height="32" source="gnome-dev-harddisk-2.png"/>
 </tile>
 <tile id="247">
  <image width="32" height="32" source="gnome-dev-harddisk-usb.png"/>
 </tile>
 <tile id="248">
  <image width="32" height="32" source="gnome-dev-harddisk.png"/>
 </tile>
 <tile id="249">
  <image width="32" height="32" source="gnome-dev-ipod.png"/>
 </tile>
 <tile id="250">
  <image width="32" height="32" source="gnome-dev-jazdisk.png"/>
 </tile>
 <tile id="251">
  <image width="32" height="32" source="gnome-dev-media-cf.png"/>
 </tile>
 <tile id="252">
  <image width="32" height="32" source="gnome-dev-media-sdmmc.png"/>
 </tile>
 <tile id="253">
  <image width="32" height="32" source="gnome-dev-memory 2.png"/>
 </tile>
 <tile id="254">
  <image width="32" height="32" source="gnome-dev-memory-large.png"/>
 </tile>
 <tile id="255">
  <image width="32" height="32" source="gnome-dev-memory.png"/>
 </tile>
 <tile id="256">
  <image width="32" height="32" source="gnome-dev-mouse-ball-small.png"/>
 </tile>
 <tile id="257">
  <image width="32" height="32" source="gnome-dev-mouse-ball.png"/>
 </tile>
 <tile id="258">
  <image width="32" height="32" source="gnome-dev-mouse-optical.png"/>
 </tile>
 <tile id="259">
  <image width="32" height="32" source="gnome-dev-pci 2.png"/>
 </tile>
 <tile id="260">
  <image width="32" height="32" source="gnome-dev-pci.png"/>
 </tile>
 <tile id="261">
  <image width="32" height="32" source="gnome-dev-pcmcia.png"/>
 </tile>
 <tile id="262">
  <image width="32" height="32" source="gnome-dev-removable.png"/>
 </tile>
 <tile id="263">
  <image width="32" height="32" source="gnome-dev-zipdisk.png"/>
 </tile>
 <tile id="264">
  <image width="32" height="32" source="gnome-fs-chardev.png"/>
 </tile>
 <tile id="265">
  <image width="32" height="32" source="gnome-fs-desktop.png"/>
 </tile>
 <tile id="266">
  <image width="32" height="32" source="gnome-fs-network.png"/>
 </tile>
 <tile id="267">
  <image width="32" height="32" source="gnome-fs-server-small.png"/>
 </tile>
 <tile id="268">
  <image width="32" height="32" source="gnome-fs-server.png"/>
 </tile>
 <tile id="269">
  <image width="32" height="32" source="gnome-fs-socket.png"/>
 </tile>
 <tile id="270">
  <image width="32" height="32" source="gnome-fs-trash-full.png"/>
 </tile>
 <tile id="271">
  <image width="32" height="32" source="gnome-joystick.png"/>
 </tile>
 <tile id="272">
  <image width="32" height="32" source="gnome-media-player.png"/>
 </tile>
 <tile id="273">
  <image width="32" height="32" source="gnome-mime-application-x-e-theme.png"/>
 </tile>
 <tile id="274">
  <image width="32" height="32" source="gnome-modem.png"/>
 </tile>
 <tile id="275">
  <image width="32" height="32" source="gnome-networktool.png"/>
 </tile>
 <tile id="276">
  <image width="32" height="32" source="gnome-other.png"/>
 </tile>
 <tile id="277">
  <image width="32" height="32" source="gnome-server-config.png"/>
 </tile>
 <tile id="278">
  <image width="32" height="32" source="gnome-starthere.png"/>
 </tile>
 <tile id="279">
  <image width="32" height="32" source="gnome-terminal.png"/>
 </tile>
 <tile id="280">
  <image width="32" height="32" source="gtk-dnd-multiple.png"/>
 </tile>
 <tile id="281">
  <image width="32" height="32" source="gtk-harddisk.png"/>
 </tile>
 <tile id="282">
  <image width="32" height="32" source="hwbrowser.png"/>
 </tile>
 <tile id="283">
  <image width="32" height="32" source="im-icq.png"/>
 </tile>
 <tile id="284">
  <image width="32" height="32" source="im-msn.png"/>
 </tile>
 <tile id="285">
  <image width="32" height="32" source="keyboard.png"/>
 </tile>
 <tile id="286">
  <image width="32" height="32" source="palm-pilot.png"/>
 </tile>
 <tile id="287">
  <image width="32" height="32" source="screensaver.png"/>
 </tile>
 <tile id="288">
  <image width="32" height="32" source="stock_addressbook.png"/>
 </tile>
 <tile id="289">
  <image width="32" height="32" source="stock_alarm.png"/>
 </tile>
 <tile id="290">
  <image width="32" height="32" source="stock_book_blue.png"/>
 </tile>
 <tile id="291">
  <image width="32" height="32" source="stock_book_red.png"/>
 </tile>
 <tile id="292">
  <image width="32" height="32" source="stock_book_yellow.png"/>
 </tile>
 <tile id="293">
  <image width="32" height="32" source="stock_briefcase.png"/>
 </tile>
 <tile id="294">
  <image width="32" height="32" source="stock_calendar.png"/>
 </tile>
 <tile id="295">
  <image width="32" height="32" source="stock_cell-phone-small.png"/>
 </tile>
 <tile id="296">
  <image width="32" height="32" source="stock_cell-phone.png"/>
 </tile>
 <tile id="297">
  <image width="32" height="32" source="stock_certificate.png"/>
 </tile>
 <tile id="298">
  <image width="32" height="32" source="stock_contact.png"/>
 </tile>
 <tile id="299">
  <image width="32" height="32" source="stock_dialog-info.png"/>
 </tile>
 <tile id="300">
  <image width="32" height="32" source="stock_edit-bookmark.png"/>
 </tile>
 <tile id="301">
  <image width="32" height="32" source="stock_insert-applet.png"/>
 </tile>
 <tile id="302">
  <image width="32" height="32" source="stock_insert-note.png"/>
 </tile>
 <tile id="303">
  <image width="32" height="32" source="stock_keyring.png"/>
 </tile>
 <tile id="304">
  <image width="32" height="32" source="stock_landline-phone-small.png"/>
 </tile>
 <tile id="305">
  <image width="32" height="32" source="stock_landline-phone.png"/>
 </tile>
 <tile id="306">
  <image width="32" height="32" source="stock_lock-open.png"/>
 </tile>
 <tile id="307">
  <image width="32" height="32" source="stock_lock.png"/>
 </tile>
 <tile id="308">
  <image width="32" height="32" source="stock_macro-watch-variable.png"/>
 </tile>
 <tile id="309">
  <image width="32" height="32" source="stock_mail-copy.png"/>
 </tile>
 <tile id="310">
  <image width="32" height="32" source="stock_mail-open.png"/>
 </tile>
 <tile id="311">
  <image width="32" height="32" source="stock_mail.png"/>
 </tile>
 <tile id="312">
  <image width="32" height="32" source="stock_network-printer.png"/>
 </tile>
 <tile id="313">
  <image width="32" height="32" source="stock_new-labels.png"/>
 </tile>
 <tile id="314">
  <image width="32" height="32" source="stock_news.png"/>
 </tile>
 <tile id="315">
  <image width="32" height="32" source="stock_notebook.png"/>
 </tile>
 <tile id="316">
  <image width="32" height="32" source="stock_notes.png"/>
 </tile>
 <tile id="317">
  <image width="32" height="32" source="stock_post-message.png"/>
 </tile>
 <tile id="318">
  <image width="32" height="32" source="stock_print.png"/>
 </tile>
 <tile id="319">
  <image width="32" height="32" source="stock_score-normal.png"/>
 </tile>
 <tile id="320">
  <image width="32" height="32" source="stock_sent-mail.png"/>
 </tile>
 <tile id="321">
  <image width="32" height="32" source="stock_snap-grid.png"/>
 </tile>
 <tile id="322">
  <image width="32" height="32" source="stock_spam.png"/>
 </tile>
 <tile id="323">
  <image width="32" height="32" source="stock_todo.png"/>
 </tile>
 <tile id="324">
  <image width="32" height="32" source="stock_toilet-paper.png"/>
 </tile>
 <tile id="325">
  <image width="32" height="32" source="stock_video-conferencing.png"/>
 </tile>
 <tile id="326">
  <image width="32" height="32" source="tsclient.png"/>
 </tile>
 <tile id="327">
  <image width="32" height="32" source="xsane.png"/>
 </tile>
 <tile id="328">
  <image width="32" height="32" source="dektops1.png"/>
 </tile>
 <tile id="329">
  <image width="32" height="32" source="dektops2.png"/>
 </tile>
 <tile id="330">
  <image width="32" height="32" source="desktops3.png"/>
 </tile>
 <tile id="331">
  <image width="32" height="32" source="desktops4.png"/>
 </tile>
 <tile id="332">
  <image width="32" height="32" source="desktops5.png"/>
 </tile>
 <tile id="333">
  <image width="32" height="32" source="desktops6.png"/>
 </tile>
 <tile id="342">
  <image width="32" height="32" source="displays1.png"/>
 </tile>
 <tile id="343">
  <image width="32" height="32" source="displays2.png"/>
 </tile>
 <tile id="344">
  <image width="32" height="32" source="displays3.png"/>
 </tile>
 <tile id="345">
  <image width="32" height="32" source="displays4.png"/>
 </tile>
 <tile id="346">
  <image width="32" height="32" source="keyboards.png"/>
 </tile>
 <tile id="347">
  <image width="32" height="32" source="keyboards2.png"/>
 </tile>
 <tile id="348">
  <image width="32" height="32" source="notebook1.png"/>
 </tile>
 <tile id="349">
  <image width="32" height="32" source="notebook2.png"/>
 </tile>
 <tile id="350">
  <image width="32" height="32" source="notebook3.png"/>
 </tile>
 <tile id="358">
  <image width="32" height="32" source="notebook4.png"/>
 </tile>
 <tile id="359">
  <image width="32" height="32" source="notebook5.png"/>
 </tile>
 <tile id="360">
  <image width="32" height="32" source="notebook6.png"/>
 </tile>
 <tile id="361">
  <image width="32" height="32" source="notebook7.png"/>
 </tile>
 <tile id="362">
  <image width="32" height="32" source="notebook8.png"/>
 </tile>
 <tile id="363">
  <image width="32" height="32" source="notebook9.png"/>
 </tile>
</tileset>
